# README #

This is the central repository for SpockMonkeys, a team for the CS 461/462 Senior Project from 2019 at Western Oregon University. The main project in these courses is meant as a capstone project for a Bachelor's degree in Computer Science. On this site you will find the following documents and code: 

* Discussion Hub
    * An in class project that was used for the initial learning of the Scrum process
* Milestones 1 through 5
    * Milestone 1 contains:
        * Resumes for each project member
        * SpockMonkeys letterhead, business cards, logo, and meet times
        * Initial concept ideas for possible project ideas
    * Milestone 2 contains: 
        * Initial inception for the in class project
        * A narrowed down, more detailed list of project ideas
    * Milestone 3 contains: 
        * Initial vision statement for Fishhawk Lake Project
        * Preliminary needs and features
        * Initial modeling
    * Milestone 4 contains:
        * Inception document for Fishhawk Lake Project, including:
            * Needs and features
            * Functional and nonfunctional requirements
            * Overall architecture design
            * Timeline and release plan
            * Final vision statement
          
## Project members ##
* Jerika Hammett
* Katelyn Rule
* Runnan Li
* Yumin Chen
          
## Fishhawk Lake Project ##

For Water Plant administrators who need an affordable, intuitive, and remote way to monitor their systems, the Water Systems Monitor Website is a user-friendly interface for plants that are set up with an economical web-enabled sensor system that compiles, displays, the data and predicts when actions should be taken. 


### Team Song ###
https://www.youtube.com/watch?v=Mwvqg8ajg8M


### Vision Statement ###

> For Water Plant administrators who need an affordable, intuitive, and remote way to monitor their systems, the Water Systems Monitor Website is a user-friendly interface for plants that are set up with an economical web-enabled sensor system that compiles, displays, the data and predicts when actions should be taken. These may include an acute leak tracking, systematic leak tracking, and high turbidity prediction. These predictions will result in warnings and suggestions given to the plant administrators. Unlike existing cloud enabled sensor systems, our system will be affordable. For those who don’t have access to the existing web software and are compiling this information by hand, we will make the data gathering process centralized and easy. 

### How do I get started?
1. Fork the repository. Follow the Bitbucket documentation [here](https://bitbucket.org/jhammett15/spockmonkeys/fork)
    * Use the blue Fork repository button
    * The format that others have used is to name it startech_*yourname* so it is easier to identify for pulls.
2. Clone the repository to your local harddrive.
    * Click the clone button near the top right. You may need to click the ... if you don't see it.
    * Copy the link.
    * Paste the link in a directory on your harddrive that does not have another repository in it.
3. Once the repository is copied. Set your upstream to the main repository.
    * On the bitbucket page, copy the string found in the top left corner of the repo's Overview page.
    * Go to a command line prompt and in the directory with the repository type
    * git remote add upstream *string you just copied*
    * This will be where you pull the latest dev branch.


### Contributing ###

For contributing and coding guildines when working on this project, see these [Guidelines](Guidelines.md).
Here is a list of [Contributors](Contributors.md) to this project. Please add your name to this document when you become a contributor.

### Software Construction Process ###

For this project, we are following Agile methodologies, specifically Disciplined Agile Delivery process framework.
We follow a two-week sprint cycle that involves daily SCRUM meetings, weekly sprint planning sessions, an end of sprint review and retrospective.
To contribute to this project, one will need to be involved in this process with the SpockMonkeys team.

### Tools, Configurations, and Packages

[Tools](Tools.md) is a list of all the software, libraries, tools, packages, and versions used on this project. Make sure you are using the same ones to avoid any compatability errors.
