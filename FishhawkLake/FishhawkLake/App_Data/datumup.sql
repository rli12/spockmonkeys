
CREATE TABLE[dbo].[DatumFromParticle]
(
    [ID] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    
    [DeviceID] NVARCHAR(64) NOT NULL,
    [Time] smalldatetime NOT NULL,
    [Value] NVARCHAR(64) NOT NULL,
    
);

GO