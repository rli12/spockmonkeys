﻿using FishhawkLake.Abstract;
using FishhawkLake.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FishhawkLake.Concrete
{
    public class AlertRepository : Repository<Alert>, IAlertRepository
    {
        public AlertRepository(FishhawkLakeDbContext context) : base(context)
        {

        }

        public FishhawkLakeDbContext FishhawkLakeDbContext
        {
            get { return Context as FishhawkLakeDbContext; }
        }

        public IEnumerable<Alert> Alerts
        {
            get { return FishhawkLakeDbContext.Alerts; }
        }

        public IEnumerable<Threshold> Thresholds
        {
            get { return FishhawkLakeDbContext.Threshold; }
        }

        public IEnumerable<Alert> GetAllAlerts()
        {
            return FishhawkLakeDbContext.Alerts.ToList();
        }

        public int GetCount()
        {
            return FishhawkLakeDbContext.Alerts.Count();
        }

        public IEnumerable<Threshold> GetAllThreshholds()
        {
            return FishhawkLakeDbContext.Threshold.ToList();
        }

        public int GetThreshholds()
        {
            return FishhawkLakeDbContext.Threshold.Count();
        }
    }
}