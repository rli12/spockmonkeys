﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishhawkLake.Models
{
    public interface IAspNetUser
    {
        bool EmailConfirmation(AspNetUser aspNetUser);
        int TotalUser(IEnumerable<AspNetUser> aspNetUsers);
    }
}
