﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FishhawkLake.Models.ViewModel
{
    public class VM
    {
        public FishhawkLakeDbContext FishhawkLakeDbContext { get; set; }
        public AspNetUser AspNetUser { get; set; }
        public IndexViewModel IndexViewModel { get; set; }
        public AspNetRole AspNetRole { get; set; }
        public AspNetUserRoles AspNetUserRoles { get; set; }
        public Device Device { get; set; }
        //public IEnumerable<Device> Devices { get; set; }
    }
}