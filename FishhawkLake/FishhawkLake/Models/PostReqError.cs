namespace FishhawkLake.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PostReqError")]
    public partial class PostReqError
    {
        public int ID { get; set; }

        [Required]
        [StringLength(128)]
        public string DATA { get; set; }

        [Required]
        [StringLength(128)]
        public string IP { get; set; }

        public DateTime Time { get; set; }
    }
}
