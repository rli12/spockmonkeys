﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FishhawkLake.Models
{
    public class PagingInfo
    {
        

        private int _TotalPages;
        private int _LastPage;
        private int _TotalItems;

        public int TotalItems
        {
            get
            {
                return _TotalItems;
            }
            set
            {
                _TotalItems = value;
            }
        }
        public int ItemsPerPage { get; set; }
        public int CurrentPage { get; set; }

        public int TotalPages
        {
            get
            {
                return (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage);
            }
            set
            {
                _TotalPages = value;
            }
        }
        public int FirstPage
        {
            get
            {
                return 1;
            }
        }
        public int LastPage
        {
            get
            {
                return TotalPages;
            }
            set
            {
                _LastPage = value;
            }
        }
    }
}