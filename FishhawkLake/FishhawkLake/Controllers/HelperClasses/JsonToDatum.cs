﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using FishhawkLake.Models;
using FishhawkLake.Controllers;


namespace FishhawkLake.Controllers.HelperClasses
{
    public class JsonToDatum
    {
        public static Boolean JObjectIsNotNull(JObject dataPoint)
        {
            if ((dataPoint != null) && !string.IsNullOrEmpty((string)dataPoint["DeviceId"]))
            {
                return true;
            }
            return false;
        }

        public static Device ToDatumNoTime(JObject dataPoint)
        {

            Device datum = new Device();
            

            if (JObjectIsNotNull(dataPoint))
            {
                datum.DeviceID = (string)dataPoint["DeviceId"];
                datum.Time = DateTime.Now;//This is bad, dont keep it- Trying to kludge together
                try
                {
                    datum.Value = double.Parse((string)dataPoint["Value"]);
                }
                catch
                {
                    return datum;
                }
            }
            return datum;
        }



        /*
        public static Boolean isValidDevice(Device Test)
        {
            //TODO: test whether it is valid. 
            return true;
        }*/
        /*
        public static DatumFromParticle ToDatumWithTime(JObject dataPoint)
        {

            Device datum = new Device();

            if (JObjectIsNotNull(dataPoint))
            {

                datum.DeviceID = (string)dataPoint["DeviceId"];
                datum.Time = (string)dataPoint["Time"];
                datum.Value = (string)dataPoint["Value"];


            }
            return datum;
        }*/


    }
}