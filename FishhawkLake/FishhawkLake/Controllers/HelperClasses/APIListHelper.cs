﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using FishhawkLake.Models;

namespace FishhawkLake.Controllers.HelperClasses
{
    public class APIListHelper
    {

        public static List<Device> GetList(string deviceID, FishhawkLakeDbContext db, int numberOfPts = 100)
        {
            if (db.Devices.Count() == 0)
            {
                return new List<Device>();
            }
            var where = db.Devices.Where(x => string.Compare(x.DeviceID, deviceID, true) == 0);
            if (where == null)
            {
                return new List<Device>();
            }
            var ordered = where.OrderByDescending(x => x.Time);
            if (ordered == null)
            {
                return new List<Device>();
            }
            var retval = ordered.Take(numberOfPts).OrderBy(x => x.Time).ToList();//top=past bottom=present ish
            if (retval == null)
            {
                return new List<Device>();
            }
            return retval;

        }

    }
}