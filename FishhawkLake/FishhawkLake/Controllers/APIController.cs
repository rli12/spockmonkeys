﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using FishhawkLake.Models;
using FishhawkLake.Controllers.HelperClasses;
using System.Diagnostics;


namespace FishhawkLake.Controllers
{
    public class APIController : ApiController
    {

        private FishhawkLakeDbContext db = new FishhawkLakeDbContext();
        // GET: api/Device/5
        // Probably more like api/API/5
        public List<Device> Get(string id)
        {
            return APIListHelper.GetList(id, db);
        }

        // POST: api/Device
        //Probably more like api/API/5
        public void Post([FromBody] JObject dataPoint)//jobject=Device
        {
            if (JsonToDatum.JObjectIsNotNull(dataPoint) && PostReqErrorsController.IsDataValid(dataPoint, db))
            {

                Device datum = JsonToDatum.ToDatumNoTime(dataPoint);
                db.Devices.Add(datum);
                db.SaveChanges();
                AlertsController alert = new AlertsController();
                alert.AlertTrigger( datum.DeviceID, datum.Value);
            }else
            {
                string ipAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

                PostReqErrorsController.ErrorToDB(dataPoint, ipAddress, db);
            }

        }


    }
}