﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FishhawkLake.Models;
using System.Diagnostics;

using Microsoft.AspNet.Identity;

namespace FishhawkLake.Controllers
{
    public class UserController : Controller
    {
        private FishhawkLakeDbContext db = new FishhawkLakeDbContext();

        private ApplicationDbContext appDb = new ApplicationDbContext();
        //ApplicationDbContext context;

        // GET: User
        public ActionResult Index()
        {

            //ViewBag.Name = new SelectList(context.Roles.Where(u => !u.Name.Contains("Admin")).ToList(), "Name", "Name");

            //var userRoles = appDb.Roles.Where(u => !u.Name.Contains("Admin")).ToList();
            //Debug.WriteLine("&*&*&*&*&*&&*&&*&*&*&*&*&*&" + userRoles);

            var userId = User.Identity.GetUserId();
            AspNetUser aspNetUser = db.AspNetUsers.Find(userId);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        public int AddOne(int x)
        {
            int y = x + 1;
            return y;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
