﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FishhawkLake.Models;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using FishhawkLake.Abstract;
using FishhawkLake.Concrete.Persistance;
using System.Web.Security;
using FishhawkLake.CustomFilters;


namespace FishhawkLake.Controllers
{
    public class PostReqErrorsController : Controller
    {
        private FishhawkLakeDbContext db = new FishhawkLakeDbContext();


        //Is Data Valid
        public static Boolean IsDataValid(JObject dataPoint, FishhawkLakeDbContext db)
        {
            Boolean valid = false;

            try
            {
                String devName = (string)dataPoint["DeviceId"];
                Double value = double.Parse((string)dataPoint["Value"]);
                int devCount = db.Threshold.Where(p => p.DeviceID == devName).Count();
                Debug.WriteLine(value+" "+devName);
                if (devCount > 0 && value > 0) //True iff find same device name in DB and value > 0;
                {
                    valid = true;
                }
                Debug.WriteLine("IsDataValid: "+valid);

            }
            catch
            {
                return false;
            }

            return valid;
        }


        /**
         * Not Valid Data to DB
         * */
        public static void ErrorToDB(JObject dataPoint, string ip, FishhawkLakeDbContext db)
        {
            var error = db.PostReqError.Create();
            string jojo;
     
            if (dataPoint != null)
            {
                jojo = dataPoint.ToString();
            }
            else
            {
                jojo = "Empty";
            }
            error.DATA = jojo;
            error.IP = ip;
            error.Time = DateTime.Now;
            db.PostReqError.Add(error);
            db.SaveChanges();
            //Debug.WriteLine(jojo);
        }



        // GET: PostReqErrors
        public ActionResult Index()
        {
            return View(db.PostReqError.ToList());
        }

        // GET: PostReqErrors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PostReqError postReqError = db.PostReqError.Find(id);
            if (postReqError == null)
            {
                return HttpNotFound();
            }
            return View(postReqError);
        }

        // GET: PostReqErrors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PostReqErrors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,DATA,IP,Time")] PostReqError postReqError)
        {
            if (ModelState.IsValid)
            {
                db.PostReqError.Add(postReqError);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(postReqError);
        }

        // GET: PostReqErrors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PostReqError postReqError = db.PostReqError.Find(id);
            if (postReqError == null)
            {
                return HttpNotFound();
            }
            return View(postReqError);
        }

        // POST: PostReqErrors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,DATA,IP,Time")] PostReqError postReqError)
        {
            if (ModelState.IsValid)
            {
                db.Entry(postReqError).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(postReqError);
        }

        // GET: PostReqErrors/Delete/5
        [AuthLogAttribute(Roles = "Admin, Manager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PostReqError postReqError = db.PostReqError.Find(id);
            if (postReqError == null)
            {
                return HttpNotFound();
            }
            return View(postReqError);
        }

        // POST: PostReqErrors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [AuthLogAttribute(Roles = "Admin, Manager")]
        public ActionResult DeleteConfirmed(int id)
        {
            PostReqError postReqError = db.PostReqError.Find(id);
            db.PostReqError.Remove(postReqError);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
