﻿window.onload = function () {
    var source = "/Home/DrawLineGraph?devID=pubpsi&dateRange=1";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: function (data) {
            var list = new Array();
            var str = data;
            //console.log(data);
            $.each(str, function (i, point) {
                list[i] = str[i].Value;
            });
            var date1 = data[0].Time;
            var date2 = data[list.length - 1].Time;

            var dateRange = getDateRange(date1, date2);

            lineG(list, "PSI", dateRange);
        },  
        error: function () {
            alert("There was an error. Please try again.");
        }
    });

    var source = "/Home/DrawLineGraph?devID=pubpsi&dateRange=7";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: function (data) {
            var list = new Array();
            var str = data;
            //console.log(data);
            $.each(str, function (i, point) {
                list[i] = str[i].Value;
            });
            var date1 = data[0].Time;
            var date2 = data[list.length - 1].Time;

            var dateRange = getDateRange(date1, date2);

            lineG(list, "PSI7", dateRange);
        },
        error: function () {
            alert("There was an error. Please try again.");
        }
    });
        
    var source = "/Home/DrawLineGraph?devID=publevel&dateRange=1";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: function (data) {
            var list = new Array();
            var str = data;
            //console.log(data);
            $.each(str, function (i, point) {
                list[i] = str[i].Value;
            });

            var date1 = data[0].Time;
            var date2 = data[list.length - 1].Time;

            var dateRange = getDateRange(date1, date2);

            lineG(list, "WaterLevel", dateRange);
        },
        error: function () {
            alert("There was an error. Please try again.");
        }
    });

    var source = "/Home/DrawLineGraph?devID=publevel&dateRange=7";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: function (data) {
            var list = new Array();
            var str = data;
            //console.log(data);
            $.each(str, function (i, point) {
                list[i] = str[i].Value;
            });

            var date1 = data[0].Time;
            var date2 = data[list.length - 1].Time;

            var dateRange = getDateRange(date1, date2);

            lineG(list, "WaterLevel7", dateRange);
        },
        error: function () {
            alert("There was an error. Please try again.");
        }
    });

    var source = "/Home/DrawLineGraph?devID=pubturbidity&dateRange=1";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: function (data) {
            var list = new Array();
            var str = data;
            //console.log(data);
            $.each(str, function (i, point) {
                list[i] = str[i].Value;
            });

            var date1 = data[0].Time;
            var date2 = data[list.length - 1].Time;

            var dateRange = getDateRange(date1, date2);

            lineG(list, "Turbidity", dateRange);
        },
        error: function () {
            alert("There was an error. Please try again.");
        }
    });

    var source = "/Home/DrawLineGraph?devID=pubturbidity&dateRange=7";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: function (data) {
            var list = new Array();
            var str = data;
            //console.log(data);
            $.each(str, function (i, point) {
                list[i] = str[i].Value;
            });

            var date1 = data[0].Time;
            var date2 = data[list.length - 1].Time;

            var dateRange = getDateRange(date1, date2);

            lineG(list, "Turbidity7", dateRange);
        },
        error: function () {
            alert("There was an error. Please try again.");
        }
    });

    var source = "/Home/DrawLineGraph?devID=pubchlorine&dateRange=1";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: function (data) {
            var list = new Array();
            var str = data;
            //console.log(data);
            $.each(str, function (i, point) {
                list[i] = str[i].Value;
            });

            var date1 = data[0].Time;
            var date2 = data[list.length - 1].Time;

            var dateRange = getDateRange(date1, date2);

            lineG(list, "Chlorine", dateRange);
        },
        error: function () {
            alert("There was an error. Please try again.");
        }
    });

    var source = "/Home/DrawLineGraph?devID=pubchlorine&dateRange=7";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: function (data) {
            var list = new Array();
            var str = data;
            //console.log(data);
            $.each(str, function (i, point) {
                list[i] = str[i].Value;
            });

            var date1 = data[0].Time;
            var date2 = data[list.length - 1].Time;

            var dateRange = getDateRange(date1, date2);

            lineG(list, "Chlorine7", dateRange);
        },
        error: function () {
            alert("There was an error. Please try again.");
        }
    });


}

function getDateRange(date1, date2) {
    var start = parseInt(date1.replace(/\/Date\((\d+)\)\//g, "$1"));
    console.log("start: " + start);

    var current = parseInt(date2.replace(/\/Date\((\d+)\)\//g, "$1"));
    console.log("current: " + current);
        
    var test = Math.round((current - start) / 86400000);
    console.log("test: " + test);

    return test;
}

function getLabelList(list, dateRange) {
    //console.log(dateRange);
    var N;
    var arr = new Array();
    if (dateRange == 1) {
        N = 24
    }
    else if (dateRange > 1 && dateRange <= 8) {
        N = 3 * dateRange;
    }
    else {
        N = dateRange;
    }

    for (var i = 0; i < N; i++) {
        arr[i] = i;
    }

    return arr;
}

function getAverages(list, arrLength) {
    var listLength = list.length;
    console.log("listLength= " + listLength);
    var chunkSize = Math.floor(listLength / arrLength);
    console.log("chunk size= " + chunkSize);
    var sum = 0;
    var newList = new Array();
    var index = 0;
    for (var i = 0; i < arrLength; i++) {

        for (var j = 0; j < chunkSize && index < listLength; j++) {
            index = (chunkSize * i) + j;
            sum += list[index];
            //console.log("index: " + index + "   sum: " + sum)
        }
        var tempAverage = sum / chunkSize;
        newList[i] = tempAverage;
        sum = 0;
        //console.log("tempAverage= " + tempAverage);
    }
    //console.log("index=" + index);
    return newList;
}

function lineG(list, label1, dateRange) {
    var arr = getLabelList(list, dateRange);
    var arrLength = arr.length;
    //console.log("arrLength= " + arrLength);
    var averages = getAverages(list, arrLength);
    

    var config = {
        type: 'line',
        data: {
            labels: arr,
            datasets: [{
                label: label1 + ' Average',
                backgroundColor: window.chartColors.red,
                borderColor: window.chartColors.red,
                data: averages,
                fill: false,
            }]
        },
        options: {
            responsive: true,

            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },

        }
    };
    var ctx = document.getElementById(label1).getContext('2d')
    window.myLine = new Chart(ctx, config);
}

