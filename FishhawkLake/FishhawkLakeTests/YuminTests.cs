﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using FishhawkLake.Models;
using FishhawkLake.Controllers;
using FishhawkLake.Controllers.HelperClasses;
using System.Web.Mvc;

namespace FishhawkLakeTests
{
    [TestClass]
    public class YuminTests
    {
        private FishhawkLakeDbContext db = new FishhawkLakeDbContext();

        [TestMethod]
        public void TextTrigTest()
        {
            AlertsController ac = new AlertsController();
            bool text = ac.TextTest();
            Assert.AreEqual(false, text);

        }

        [TestMethod]
        public void PhoneTrigTest()
        {
            AlertsController ac = new AlertsController();
            bool text = ac.PhoneTest();
            Assert.AreEqual(false, text);

        }

        [TestMethod]
        public void EmailTrigTest()
        {
            AlertsController ac = new AlertsController();
            bool text = ac.EmailTest();
            Assert.AreEqual(false, text);

        }

        [TestMethod]
        public void AlertIndexTest()
        {
            AlertsController ac = new AlertsController();
            ActionResult result = ac.Index();
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

       [TestMethod]
       public void DashboardTest()
        {
            AlertsController ac = new AlertsController();
            ActionResult dashboard = ac.Dashboard();
            Assert.IsInstanceOfType(dashboard, typeof(ViewResult));
        }

        [TestMethod]
        public void AlertTriggerTest()
        {
            AlertsController ac = new AlertsController();
            ActionResult tri = ac.AlertTrigger("pubPsi",10);
            Assert.IsInstanceOfType(tri, typeof(ViewResult));
        }
    }
}
