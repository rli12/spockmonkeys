﻿using System;
using FishhawkLake.Abstract;
using FishhawkLake.Controllers;
using FishhawkLake.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FishhawkLakeTests
{
    [TestClass]
    public class JerikasAlarmLogTests
    {
        [TestMethod]
        public void CanReturnIndex()
        {
            // arrange
            Mock<IAlertRepository> mock = new Mock<IAlertRepository>();
            mock.Setup(m => m.Alerts).Returns(new Alert[]
            {
                new Alert {ID = 1, Value = 77.7, DeviceID = "pubPsi" },
                new Alert {ID = 2, Value = 78.7, DeviceID = "pubPsi" },
                new Alert {ID = 3, Value = 80, DeviceID = "pubPsi" },
                new Alert {ID = 4, Value = 82.1, DeviceID = "pubPsi" },
            });

            // act
            AlertsController controller = new AlertsController(mock.Object);
            var list = controller.AlertLog();

            // assert

        }
    }
}
