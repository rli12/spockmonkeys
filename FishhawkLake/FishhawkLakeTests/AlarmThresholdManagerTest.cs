﻿using System;
using FishhawkLake.Controllers.HelperClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FishhawkLakeTests
{
    [TestClass]
    public class AlarmThresholdManagerTest
    {
        [TestMethod]
        public void HasPassedThreshold_TriggersForPSI()
        {
            Assert.IsTrue(AlarmThresholdManager.HasPassedThreshold("pubpsi", 80));
        }
        [TestMethod]
        public void HasPassedThreshold_TriggersForTurb()
        {
            Assert.IsTrue(AlarmThresholdManager.HasPassedThreshold("pubturbidity", 3.05));
        }
        [TestMethod]
        public void HasPassedThreshold_TriggersForChlor()
        {
            Assert.IsTrue(AlarmThresholdManager.HasPassedThreshold("pubchlorine", 1.05));
        }

    }
}
